## About
A simple contacts app where you can search and create/update/delete contacts.

DEMO (video): https://drive.google.com/file/d/1xS2vW3D4LQEGBzV4ruAVIOxdSJLSE9od/view?usp=sharing

## Launch

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[JSON-server](https://github.com/typicode/json-server) is used as a backed API.

To run the project enter the commands below:

`npm i -g json-server`

`json-server db.json`

In another terminal:

`npm i`

`npm start`

It uses port 3000 for client and 3001 for server.


## Tests
Sorry but I don't have time now to write tests now. I had 1 week for this task. I can add tests later if needed. Thanks.


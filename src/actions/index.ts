import { createAction } from 'redux-actions';
import { Dispatch } from 'redux';
import * as api from '../services/api';
import IContact from '../interfaces/IContact';

export const setLoading = createAction('setLoading');
export const setContacts = createAction('setContacts');
export const addContacts = createAction('addContacts');
export const deleteContact = createAction('deleteContact');
export const updateContact = createAction('updateContact');

function handleError(error: Error) {
  alert(error)
}

export const loadContactsFromServer = () => (dispatch: Dispatch) => {
  dispatch(setLoading(true));

  return api.loadContactsFromServer()
    .then(response => response.json().then(contacts => {
      dispatch(setContacts(contacts));
      dispatch(setLoading(false));

      return contacts;
    }))
    .catch(error => handleError(error))
};

export const loadContactFromServer = (id: string) => (dispatch: Dispatch) => {
  dispatch(setLoading(true));

  return api.loadContactFromServer(id)
    .then(response => response.json().then(contact => {
      dispatch(addContacts([contact]));
      dispatch(setLoading(false));

      return contact;
    }))
    .catch(error => handleError(error))
};

export const deleteContactOnServer = (contact: IContact) => (dispatch: Dispatch) => {
  dispatch(setLoading(true));

  return api.deleteContactOnServer(contact)
    .then(() => {
      dispatch(deleteContact(contact));
      dispatch(setLoading(false));
    })
    .catch(error => handleError(error));
};

export const updateContactOnServer = (contact: IContact) => (dispatch: Dispatch) => {
  dispatch(setLoading(true));

  return api.updateContactOnServer(contact)
    .then(response => response.json())
    .then(updatedContact => {
      dispatch(updateContact(updatedContact));
      dispatch(setLoading(false));

      return updatedContact;
    })
    .catch(error => handleError(error))
};

export const createContactOnServer = (contact: IContact) => (dispatch: Dispatch) => {
  dispatch(setLoading(true));

  return api.createContactOnServer(contact)
    .then(response => response.json())
    .then(createdContact => {
      dispatch(addContacts([createdContact]));
      dispatch(setLoading(false));

      return createdContact;
    })
    .catch(error => handleError(error))
};

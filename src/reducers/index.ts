import { handleActions } from 'redux-actions';
import * as actions from '../actions';
import IState from '../interfaces/IState';
import IAction from '../interfaces/IAction';
import IContact from '../interfaces/IContact';

export default handleActions({
  [`${actions.setLoading}`]: (state: IState, action: IAction<any>) => ({
    ...state,
    loading: action.payload
  }),
  [`${actions.setContacts}`]: (state: IState, action: IAction<IContact[]>) => ({
    ...state,
    contacts: action.payload
  }),
  [`${actions.deleteContact}`]: (state: IState, action: IAction<IContact>) => ({
    ...state,
    contacts: state.contacts.filter(c => c.id !== action.payload.id)
  }),
  [`${actions.addContacts}`]: (state: IState, action: IAction<IContact[]>) => ({
    ...state,
    contacts: state.contacts
      .filter(contact =>
        action.payload.every((payloadContact: IContact) => payloadContact.id !== contact.id)
      )
      .concat(action.payload)
  }),
  [`${actions.updateContact}`]: (state: IState, action: IAction<IContact>) => ({
    ...state,
    contacts: state.contacts.map(c => c.id === action.payload.id ? action.payload : c)
  }),
} as any, { contacts: [], loading: true });

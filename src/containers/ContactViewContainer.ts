import { connect } from 'react-redux';
import ContactView from '../components/ContactView';
import IState from '../interfaces/IState';
import { deleteContactOnServer, loadContactFromServer } from '../actions';

export default connect(
  (state: IState, ownProps: { match: { params: { id: string } } }) => {
    return {
      contact: state.contacts.find(c => c.id === ownProps.match.params.id),
      loading: state.loading
    }
  },
  {
    deleteContactOnServer,
    loadContactFromServer
  }
)(ContactView);

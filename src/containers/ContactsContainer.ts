import { connect } from 'react-redux';
import Contacts from '../components/Contacts';
import IState from '../interfaces/IState';
import { loadContactsFromServer } from '../actions';

export default connect(
  (state: IState) => ({
    contacts: state.contacts,
    loading: state.loading
  }),
  {
    loadContactsFromServer
  }
)(Contacts);

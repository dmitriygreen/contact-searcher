import { connect } from 'react-redux';
import Form from '../components/Form';
import IState from '../interfaces/IState';
import { loadContactFromServer, updateContactOnServer, createContactOnServer } from '../actions';

export default connect(
  (state: IState, ownProps: { match: { params: { id: string } } }) => {
    return {
      contact: state.contacts.find(c => c.id === ownProps.match.params.id),
      loading: state.loading
    }
  },
  {
    loadContactFromServer,
    updateContactOnServer,
    createContactOnServer
  }
)(Form);

export default interface IContact {
  id: string;
  name: string;
  lastName: string;
  email: string;
  mobileNumber: string;
  notes?: string;
}

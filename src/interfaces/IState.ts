import IContact from './IContact';

export default interface IState {
  contacts: IContact[];
  loading: boolean;
}

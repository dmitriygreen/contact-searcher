import React from 'react';
import Link from '../Link';
import { Link as RouterLink } from 'react-router-dom';
import IContact from '../../interfaces/IContact';
import PageTitle from '../PageTitle';
import styles from './Contacts.module.scss';

interface IProps {
  contacts: IContact[];
  loading: boolean;
  loadContactsFromServer(): void;
}

interface IState {
  searchValue: string;
}

class Contacts extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      searchValue: ''
    };

    this.handleSearchInputChange = this.handleSearchInputChange.bind(this);
  }

  componentDidMount() {
    this.props.loadContactsFromServer();
  }

  handleSearchInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ searchValue: event.target.value })
  }

  renderContactList() {
    const { searchValue } = this.state;

    return this.props.contacts
      .filter(c => (`${c.name.toLowerCase()} ${c.lastName.toLowerCase()}`).includes(searchValue.toLowerCase()))
      .sort((a, b) => a.name + a.lastName > b.name + b.lastName ? 1 : -1)
      .map(({ id, name, lastName }) => (
        <li className={styles.contact} key={id}>
          <RouterLink to={`/contacts/${id}`} className={styles.contactLink}>{name} {lastName}</RouterLink>
        </li>
      ));
  }

  render() {
    const { loading } = this.props;
    const { searchValue } = this.state;
    const contactList = this.renderContactList();

    return (
      <div className={styles.contacts}>
        <PageTitle title='Contacts' loading={loading}>
          <Link className={styles.createContactLink} to='/newContact'>Create</Link>
        </PageTitle>
        <input
          className={styles.searchInput}
          value={searchValue}
          onChange={this.handleSearchInputChange}
        />
        <ul className={styles.list}>{contactList}</ul>
      </div>
    )
  }
}

export default Contacts;
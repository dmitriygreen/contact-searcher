import React from 'react';
import classNames from 'classnames';
import styles from './Button.module.scss';

interface IProps {
  className?: string;
  type?: 'button' | 'submit';
  bordered?: boolean;
  onClick?(event: React.MouseEvent<HTMLButtonElement>): void;
}

const Button: React.FC<IProps> = ({ children, className, type, bordered, onClick }) => (
  <button
    className={classNames(styles.button, className, { [styles.bordered]: bordered })}
    type={type}
    onClick={onClick}
  >
    {children}
  </button>
);

Button.defaultProps = {
  type: 'button',
  bordered: false
};

export default Button;

import React from 'react';
import styles from './PageTitle.module.scss';
import Preloader from '../Preloader/Preloader';

interface IProps {
  title: string;
  loading?: boolean;
}

const PageTitle: React.FC<IProps> = ({ children, title, loading }) => (
  <div className={styles.pageTitle}>
    <h2 className={styles.titleText}>{title}</h2>
    {loading && <Preloader />}
    {children}
  </div>
);

PageTitle.defaultProps = {
  loading: false
};

export default PageTitle;

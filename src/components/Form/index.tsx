import React from 'react';
import PageTitle from '../PageTitle';
import styles from './Form.module.scss';
import Field from './Field';
import IContact from '../../interfaces/IContact';
import { RouteComponentProps } from 'react-router';
import Button from '../Button';
import Link from '../Link';
import {
  validateEmail,
  validateLastName,
  validateMobileNumber,
  validateName,
  validateNotes
} from '../../services/validation';

interface IProps extends RouteComponentProps<{ id: string }> {
  contact?: IContact;
  loading: boolean;
  loadContactFromServer(id: string): any;
  updateContactOnServer(contact: IContact): Promise<IContact>;
  createContactOnServer(contact: IContact): Promise<IContact>;
}

interface IState {
  contact: IContact;
  invalidFields: string[];
  submitted: boolean;
}

class Form extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.isFieldInvalid = this.isFieldInvalid.bind(this);

    this.state = {
      contact: props.contact || {
        id: '',
        name: '',
        lastName: '',
        email: '',
        mobileNumber: ''
      },
      invalidFields: [],
      submitted: false
    }
  }

  static getDerivedStateFromProps(props: IProps, state: IState) {
    if (state.contact.id.length === 0 && props.contact) {
      return {
        ...state,
        contact: props.contact
      };
    } else {
      return null;
    }
  }

  componentDidMount() {
    if (!this.props.contact && this.props.match.params.id) {
      this.props.loadContactFromServer(this.props.match.params.id)
    }
  }

  onChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      contact: {
        ...this.state.contact,
        [event.target.name]: event.target.value
      }
    }, () => {
      if (this.state.submitted) {
        this.validateForm();
      }
    });
  }

  validateForm(): boolean {
    const { name, lastName, email, mobileNumber, notes } = this.state.contact;
    const invalidFields: string[] = [];

    if (!validateName(name)) {
      invalidFields.push('name');
    }

    if (!validateLastName(lastName)) {
      invalidFields.push('lastName');
    }

    if (!validateEmail(email)) {
      invalidFields.push('email');
    }

    if (!validateMobileNumber(mobileNumber)) {
      invalidFields.push('mobileNumber');
    }

    if (!validateNotes(notes)) {
      invalidFields.push('notes');
    }

    this.setState({ invalidFields });

    return invalidFields.length === 0;
  }

  isFieldInvalid(fieldName: string): boolean {
    return this.state.invalidFields.includes(fieldName);
  }

  onSubmit(event: React.FormEvent) {
    event.preventDefault();
    this.setState({ submitted: true });

    if (!this.validateForm()) {
      return;
    }

    const { contact } = this.state;
    const { params } = this.props.match;
    const promise = params.id ? this.props.updateContactOnServer(contact) : this.props.createContactOnServer(contact);

    promise.then((createdContact) => {
      this.props.history.push(`/contacts/${createdContact.id}`);
    });
  }

  render() {
    const { id, name, lastName, email, mobileNumber, notes } = this.state.contact;
    const { match, loading } = this.props;

    return (
      <div>
        <PageTitle title={match.params.id ? 'Edit contact' : 'New contact'} loading={loading} />
        <form className={styles.form} onSubmit={this.onSubmit}>
          <Field
            name='name'
            label='Name'
            value={name}
            invalid={this.isFieldInvalid('name')}
            onChange={this.onChange}
          />
          <Field
            name='lastName'
            label='Last name'
            value={lastName}
            invalid={this.isFieldInvalid('lastName')}
            onChange={this.onChange}
          />
          <Field
            name='email'
            label='Email'
            value={email}
            type='email'
            invalid={this.isFieldInvalid('email')}
            onChange={this.onChange}
          />
          <Field
            name='mobileNumber'
            label='Mobile number'
            value={mobileNumber}
            type='tel'
            invalid={this.isFieldInvalid('mobileNumber')}
            onChange={this.onChange}
          />
          <Field
            name='notes'
            label='Notes'
            value={notes}
            invalid={this.isFieldInvalid('notes')}
            onChange={this.onChange}
          />
          <div className={styles.navActions}>
            <Link to={id.length === 0 ? '/' : `/contacts/${id}`}>Back</Link>
            <Button type='submit'>Save</Button>
          </div>
        </form>
      </div>
    );
  }
}

export default Form;

import React from 'react';
import styles from './Form.module.scss';
import classNames from 'classnames';

interface IProps {
  name: string;
  label: string;
  value?: string;
  type?: string;
  invalid?: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Field: React.FC<IProps> = ({ name, label, value, type, invalid, onChange }) => (
  <label className={styles.label}>
    <span className={styles.labelText}>{label}</span>
    <input
      type={type}
      name={name}
      className={classNames(styles.textInput, { [styles.invalid]: invalid })}
      value={value}
      onChange={onChange}
    />
  </label>
);

Field.defaultProps = {
  type: 'text',
  value: '',
  invalid: false
};

export default Field;

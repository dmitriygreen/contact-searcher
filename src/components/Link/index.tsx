import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import classNames from 'classnames';
import styles from './Link.module.scss';

interface IProps {
  to: string;
  className?: string;
  bordered?: boolean;
  onClick?(event: React.MouseEvent<HTMLButtonElement>): void;
}

const Link: React.FC<IProps> = ({ children, className, to, bordered }) => (
  <RouterLink
    to={to}
    className={classNames(styles.link, className, { [styles.bordered]: bordered })}
  >
    {children}
  </RouterLink>
);

Link.defaultProps = {
  bordered: false
};

export default Link;

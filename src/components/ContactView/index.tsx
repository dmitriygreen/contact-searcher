import React from 'react';
import classNames from 'classnames';
import { Link, RouteComponentProps } from 'react-router-dom';
import Button from '../Button';
import IContact from '../../interfaces/IContact';
import styles from './ContactView.module.scss';
import PageTitle from '../PageTitle';
import Preloader from '../Preloader/Preloader';

interface IProps extends RouteComponentProps<{ id: string }> {
  id?: string;
  contact?: IContact;
  loading: boolean;
  deleteContactOnServer(contact: IContact): Promise<void>;
  loadContactFromServer(id: string): void;
}

class ContactView extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props);
    this.deleteContact = this.deleteContact.bind(this);
    this.requestDeleteContactConfirmation = this.requestDeleteContactConfirmation.bind(this);
  }

  componentDidMount() {
    if (!this.props.contact) {
      this.props.loadContactFromServer(this.props.match.params.id)
    }
  }

  deleteContact() {
    const { contact } = this.props;

    if (contact) {
      this.props.deleteContactOnServer(contact).then(() => {
        this.props.history.push('/');
      })
    }
  };

  requestDeleteContactConfirmation() {
    const { contact } = this.props;

    if (contact) {
      const { name, lastName } = contact;
      const result = window.confirm(`Are you sure you want to delete the contact "${name} ${lastName}"?`);

      if (result) {
        this.deleteContact()
      }
    }
  }

  render() {
    const { contact, loading } = this.props;

    if (!contact) {
      return loading ? <Preloader /> : 'Contact not found';
    }

    const { id, name, lastName, email, mobileNumber, notes } = contact;

    return (
      <div className={styles.contact}>
        <PageTitle title={`${name} ${lastName}`} loading={loading} />
        <ul className={styles.info}>
          {email && (
            <li className={classNames(styles.infoItem, styles.email)}>
              <a href={`mailto:${email}`} className={styles.link}>{email}</a>
            </li>
          )}
          {mobileNumber && (
            <li className={classNames(styles.infoItem, styles.mobileNumber)}>
              <a
                href={`tel:${mobileNumber}`}
                className={classNames(styles.link, styles.numberLink)}
              >
                {mobileNumber}
              </a>
            </li>
          )}
          {notes && (
            <li className={classNames(styles.infoItem, styles.notes)}>
              Notes: {notes}
            </li>
          )}
          <li className={styles.infoItem}>
            <Link to={`/contacts/${id}/edit`} className={styles.link}>Edit contact</Link>
          </li>
          <li className={styles.infoItem}>
            <Button onClick={this.requestDeleteContactConfirmation}>
              Delete contact
            </Button>
          </li>
        </ul>
        <div className={styles.navActions}>
          <Link to='/' className={classNames(styles.link, styles.navLink)}>Back to contacts</Link>
        </div>
      </div>
    )
  }
}

export default ContactView;

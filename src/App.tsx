import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import styles from './App.module.scss';
import Contacts from './containers/ContactsContainer';
import ContactView from './containers/ContactViewContainer';
import Form from './containers/FormContainer';

const App: React.FC = () => {
  return (
    <Router>
      <div className='app'>
        <header>
          <h1 className={styles.title}>Contact Searcher</h1>
        </header>
        <div className={styles.appContainer}>
          <Route exact path='/' component={Contacts} />
          <Route exact path='/contacts/:id' component={ContactView}  />
          <Route exact path='/contacts/:id/edit' component={Form} />
          <Route exact path='/newContact' component={Form} />
        </div>
      </div>
    </Router>
  );
};

export default App;

import { API_URL } from '../../constants';
import IContact from '../../interfaces/IContact';

interface FetchApiOptions {
  method?: string;
  body?: any;
  headers?: Object;
}

export function fetchApi(url: string, options: any = {}) {
  const defaultOptions: FetchApiOptions = {};

  if (options.method && ['POST', 'PUT', 'PATCH'].includes(options.method.toUpperCase())) {
    defaultOptions.headers = {
      'Content-Type': 'application/json',
      ...(options.headers || {})
    }
  }

  return fetch(API_URL + url, { ...defaultOptions, ...options});
}

export function loadContactsFromServer() {
  return fetchApi('/contacts');
}

export function loadContactFromServer(id: string) {
  return fetchApi(`/contacts/${id}`);
}

export function deleteContactOnServer(contact: IContact) {
  return fetchApi(`/contacts/${contact.id}`, { method: 'DELETE' });
}

export function updateContactOnServer(contact: IContact) {
  return fetchApi(`/contacts/${contact.id}`, { method: 'PATCH', body: JSON.stringify(contact) });
}

export function createContactOnServer(contact: IContact) {
  return fetchApi(`/contacts`, { method: 'POST', body: JSON.stringify(contact) });
}
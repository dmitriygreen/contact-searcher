const STRING_MAX_LENGTH = 255;

export function validateName(name: string) {
  return name && name.length > 0 && name.length < STRING_MAX_LENGTH;
}

export function validateLastName(lastName: string) {
  return validateName(lastName);
}

export function validateNotes(note?: string) {
  return !note || note.length < STRING_MAX_LENGTH;
}

export function validateEmail(email: string) {
  const regExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return !email || regExp.test(email);
}

export function validateMobileNumber(mobileNumber: string) {
  const regExp = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-. \\\/]?)?((?:\(?\d{1,}\)?[\-. \\\/]?){0,})(?:[\-. \\\/]?(?:#|ext\.?|extension|x)[\-. \\\/]?(\d+))?$/i;

  return !mobileNumber || regExp.test(mobileNumber);
}

